;;; alight-mode.el --- 

;; Copyright 2022 cnngimenez
;;
;; Author: cnngimenez
;; Version:
;; Keywords: 
;; X-URL: not distributed yet

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; 

;; Put this file into your load-path and the following into your ~/.emacs:
;;   (require 'alight-mode)

;;; Code:

(require 'newcomment)
(require 'prog-mode)
(require 'alight-commands)

(defconst alight-reserved-words
  '("abort" "else" "new" "return"
    "abs" "elsif" "not" "reverse"
    "abstract" "end" "null"
    "accept" "entry" "select"
    "access" "exception" "of" "separate"
    "aliased" "exit" "or" "some"
    "all" "others" "subtype"
    "and" "for" "out" "synchronized"
    "array" "function" "overriding"
    "at" "tagged"
    "generic" "package" "task"
    "begin" "goto" "parallel" "terminate"
    "body" "pragma" "then"
    "if" "private" "type"
    "case" "in" "procedure"
    "constant" "interface" "protected" "until"
    "is" "use"
    "declare" "raise"
    "delay" "limited" "range" "when"
    "delta" "loop" "record" "while"
    "digits" "rem" "with"
    "do" "mod" "renames"
    "requeue" "xor")
  "A list of Ada reserved words.")

(defconst alight-reserved-words-regexp
  (regexp-opt alight-reserved-words 'symbols)
  "A Regexp made with `alight-reserved-words'.")

(defconst alight-type-keywords-regexp
  (regexp-opt '("String" "Character"
                "Bounded_String" "Unbounded_String"
                "Wide_String" "Wide_Character"
                "Bounded_Wide_String" "Unbounded_Wide_String"
                "Wide_Wide_String" "Wide_Wide_Character"
                "Bounded_Wide_Wide_String" "Unbounded_Wide_Wide_String"
                "Integer" "Positive" "Natural"
                "Duration"
                "Boolean")
              'symbols)
  "Regexp for comon types keywords." )

(defconst alight-subprogram-regexp
  "\\(procedure\\|function\\)[[:space:]]+\\(.*\\)[[:space:]]*\\(return\\|is\\|;\\)"  
  "Regexp to match against subprogram definitions
Ex.: procedure MyProcedure (Parameters);")

(defconst alight-font-lock-keywords
  `((,alight-reserved-words-regexp (0 font-lock-keyword-face))
    (,alight-type-keywords-regexp (0 font-lock-type-face))
    (,alight-subprogram-regexp (0 font-lock-function-name-face t)))
  "Expressions to highlight in A-Light mode.")

(defun alight-indent-line ()
  "Indent line or move cursor.
Rules:
1. If cursor is at the beginning, move it to the first letters.
2. If first character is not in the same column as the line before: indent to
   last column.
3. If cursor is at the "
  (interactive)
  (let ((last-indent (alight--find-last-indent)))
    (cond
     ((and (< (current-column) last-indent)
           (< (current-indentation) last-indent))
      ;; Cursor is not in the last indent and indentation is not the same as
      ;; the last one.  Move them at last indent!
      (indent-to last-indent)
      (goto-char (+ (point-at-bol) last-indent)))
     ((and (< (current-column) last-indent)
           (>= (current-indentation) last-indent))
      ;; Cursor is not in the indent -> move it.
      (goto-char (+ (point-at-bol) last-indent)))
     ((= (current-column) (current-indentation))
           (save-excursion
             (goto-char (point-at-bol))
             (indent-to (+ (current-column) 4))))     
     (t
      (indent-to last-indent)))

    (when (= (current-column) 0)
      (goto-char (+ (point-at-bol) (current-indentation)))))
  
  ;; Consider when the line is empty the user wants to add space:
  ;; cursor should be at the EOL.
  (when (looking-at "^[[:space:]]*$")
    (goto-char (point-at-eol))))

(defun alight-indent-line-backwards ()
  (interactive)
  (save-excursion
    (goto-char (point-at-bol))
    (when (looking-at "    ")
      (delete-char 4))))

;; --------------------------------------------------

;; From Ada-mode
(defvar alight-mode-syntax-table
  (let ((table (make-syntax-table)))
    ;; (info "(elisp)Syntax Class Table" "*info syntax class table*")
    ;; (info "(elisp) Syntax Flags") for comment start/end
    ;; make-syntax-table sets all alphanumeric to w, etc; so we only
    ;; have to add ada-specific things.

    ;; string brackets. `%' is the obsolete alternative string
    ;; bracket (arm J.2); if we make it syntax class ", it throws
    ;; font-lock and indentation off the track, so we use syntax class
    ;; $.
    (modify-syntax-entry ?%  "$" table)
    (modify-syntax-entry ?\" "\"" table)

    ;; punctuation; operators etc
    (modify-syntax-entry ?#  "." table); based number
    (modify-syntax-entry ?&  "." table)
    (modify-syntax-entry ?*  "." table)
    (modify-syntax-entry ?+  "." table)
    (modify-syntax-entry ?-  ". 124" table); operator, double hyphen as comment
    (modify-syntax-entry ?. "." table)
    (modify-syntax-entry ?/  "." table)
    (modify-syntax-entry ?:  "." table)
    (modify-syntax-entry ?<  "." table)
    (modify-syntax-entry ?=  "." table)
    (modify-syntax-entry ?>  "." table)
    (modify-syntax-entry ?@  "." table)
    (modify-syntax-entry ?\' "." table); attribute; see ada-syntax-propertize for character literal
    (modify-syntax-entry ?\; "." table)
    (modify-syntax-entry ?\\ "." table); default is escape; not correct for Ada strings
    (modify-syntax-entry ?\|  "." table)

    ;; \f and \n end a comment.
    (modify-syntax-entry ?\f  ">" table)
    (modify-syntax-entry ?\n  ">" table)

    (modify-syntax-entry ?_ "_" table); symbol constituents, not word.

    (modify-syntax-entry ?\( "()" table)
    (modify-syntax-entry ?\) ")(" table)

    ;; skeleton placeholder delimiters; see ada-skel.el. We use generic
    ;; comment delimiter class, not comment starter/comment ender, so
    ;; these can be distinguished from line end.
    (modify-syntax-entry ?{ "!" table)
    (modify-syntax-entry ?} "!" table)

    table
    )
  "Syntax table to be used for editing Ada source code.")

;; --------------------------------------------------
                                        ; Keyboard

(defun alight-char-key (char)
  "Insert CHAR but fix indent before."
  (alight-fix-case-at-point)
  (insert char))

(defun alight-space-key ()
  "Insert space and do other things...
When spacing it would be useful to fix some things before insterting the actual
space.  For instance, fix cases when using identifiers. "
  (interactive)
  (alight-char-key " "))

(defun alight-return-key ()
  "Insert return and do other things..."
  (interactive)
  (alight-fix-case-at-point)
  (newline)
  (indent-for-tab-command))

(defun alight-semi-key ()
  "Insert a semicolon and fix down/upcase."
  (interactive)
  (alight-char-key ";"))

(defun alight-parent-key ()
  "Insert a semicolon and fix down/upcase."
  (interactive)
  (alight-char-key ")"))

(defun alight-dot-key ()
  "Insert a dot and fix down/upcase."
  (interactive)
  (alight-char-key "."))

(defun alight-apostrophe-key ()
  "Insert an apostrophe and fix down/upcase."
  (interactive)
  (alight-char-key "'"))

(defvar alight-mode-map
  (let ((map (make-sparse-keymap)))
    ;; (define-key map "\C-c;" 'alight-function)
    (define-key map [return] #'alight-return-key)
    (define-key map "\t" #'alight-indent-line)
    (define-key map "'" #'alight-apostrophe-key)
    (define-key map "." #'alight-dot-key)
    (define-key map ")" #'alight-parent-key)
    (define-key map ";" #'alight-semi-key)
    (define-key map " " #'alight-space-key)
    ;; Inserting templates
    (define-key map "\C-c\C-e" #'alight-insert-end)
    (define-key map "\C-c\C-b" #'alight-insert-subprogram-body)
    ;; Goto keys
    (define-key map "\M-gb" #'alight-goto-package-body)
    (define-key map "\M-gs" #'alight-goto-package-specification)
    (define-key map "\M-gl" #'alight-find-library-at-point)
    ;; Indent
    (define-key map [backtab] #'alight-indent-line-backwards)
    map)
  "Keymap used for  `alight-mode'.")

;; --------------------------------------------------

(define-derived-mode alight-mode prog-mode "A-Light"
  "Ada Light (A-Light) mode."

  :group 'alight
  
  (setq-local parse-sexp-ignore-comments t)

  (setq-local case-fold-search t)
  (setq-local completion-ignore-case t)

  ;; Comments
  ;; (setq-local comment-start "--")
  ;; (setq-local comment-end "")
  ;; ;; (setq-local comment-start-skip "---*[ \t]*") ;; from ada-mode
  
  (setq-local comment-multi-line nil)
  (setq-local comment-padding "  ") ;; comments should have two spaces: "--  "
  (setq-local comment-start "--")
  (setq-local comment-start-skip "\\(?://+\\|/\\*+\\)\\s *")
  (setq-local comment-end "")

  (setq-local require-final-newline t)

  ;; Indentation should be 4 spaces, no tab.
  ;; (setq-local indent-line-function #'alight-indent-line)
  (setq-local indent-tabs-mode nil)
  (setq-local standard-indent 4)

  ;; Imenu
  (setq-local imenu-generic-expression
              '(
                ("types" "\\(^\\|[[:space:]]+\\|;\\)type[[:blank:]]+\\([[:alnum:]_]+\\)" 2)
                ("procedures" "procedure[[:blank:]]+\\([[:alnum:]_]+\\)" 1)
                ("functions" "function[[:blank:]]+\\([[:alnum:]_]+\\)" 1)
                ("packages" "package[[:blank:]]+\\(body[[:blank:]]+\\)?\\([[:alnum:]\\\\._]+\\)" 2)))
                                         
  
  ;; Font lock support
  (setq-local font-lock-defaults '(alight-font-lock-keywords
                                   nil
                                   t
                                   ((?\_ . "w"))))

  ;; Fix before saving
  (add-hook 'before-save-hook #'alight-fix-style nil t))

(add-to-list 'auto-mode-alist '("\\.ad[abs]\\'" . alight-mode))


(provide 'alight-mode)

;;; alight-mode.el ends here
