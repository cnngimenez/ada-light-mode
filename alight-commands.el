;;; alight-commands.el --- Useful commands for Ada Light mode.

;; Copyright 2022 cnngimenez
;;
;; Author: cnngimenez
;; Version:
;; Keywords: 
;; X-URL: not distributed yet

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; 

;; Put this file into your load-path and the following into your ~/.emacs:
;;   (require 'alight-commands)

;;; Code:

;; --------------------------------------------------
;; Finding Ada libraries in system

(defun alight-library-filename (library-name)
  "Return the Ada library name of LIBRARY-NAME.
From a complete name, return its filename.  For instance:
Ada.Text_IO return a-textio.ads."
  (let* ((downcase-name (downcase library-name))
         (names (split-string downcase-name "\\(\\.\\|-\\|_\\)")))
    (cond
     ((= (length names) 1)
      (format "%s.ads" (if (> (length downcase-name) 6)
                           (substring downcase-name 0 6)
                         library-name)))
     ((= (length names) 2)
      (format "%s-%s.ads"
              (substring (car names) 0 1)
              (substring (cadr names) 0 6)))
     ((>= (length names) 3)
      (format "%s-%s%s.ads"
                  (substring (car names) 0 1)
              (substring (cadr names) 0 3)
              (substring (nth 2 names) 0 3))))))

(defun alight-find-include-paths ()
  "Return where are the Ada include path in the system.

Return a list of paths with the adainclude directory."
  (file-expand-wildcards "/usr/lib/gcc/*/*/adainclude"))

(defun alight-find-include-path-this-system ()
  "Return where are the Ada include paths useful for this system."
  (car (file-expand-wildcards
        (format "/usr/lib/gcc/%s/*/adainclude" system-configuration))))

(defun alight-find-all-include-libraries ()
  "Return a list of all include libraries."
  (directory-files (alight-find-include-path-this-system) nil ".*\\.ads"))

(defun alight-find-library (library-name &optional no-check)
  "Return the Ada library path.
LIBRARY-NAME is an Ada library compltee name, or a filename.
Given an Ada library name, search for its filename.

If NO-CHECK is t, do not check for file existence."
  (let ((library-path (format "%s/%s"
                              (alight-find-include-path-this-system)
                              (alight-library-filename library-name))))
    (if no-check
        library-path
      (when (file-exists-p library-path)
        library-path))))

(defun alight-find-file-library (library-name)
  "Find the Ada interface library.
Given the LIBRARY-NAME, search it under /usr/lib and return it."
  (interactive (list
                (completing-read "Library name?"
                                 (mapcar (lambda (name)
                                           "Remove the \".ads\" suffix."
                                           (substring name 0 -4))
                                         (alight-find-all-include-libraries))
                                 nil nil)))

  (let ((library-path (alight-find-library library-name)))
    (if library-path
        (find-file-other-window library-path)
      (message "Library %s cannot be found!" library-name))))

(defun alight-sublibraries (str)
  "Return a list of libraries and sublibraries of STR.
For instance:
If STR is Ada.Characters.Handlers, returns a list with Ada.Characters.Handlers
Ada.Characters, and Ada."
  (let ((names (split-string str  "\\."))
        (result nil))   
    (dolist (n (number-sequence 1 (length names)))
      (push (mapconcat (lambda (s) s)
                       (take n names)
                       ".")
            result))
    result))      

(defun alight-find-library-at-point ()
  "Find the library file in your system.

Search the library name at point, ask the user which library name to use, and
open the file." 
  (interactive)
  (save-excursion
    (re-search-backward "\\($\\|[[:space:]]\\|(\\)" (line-beginning-position) t)
    (right-char)
    (looking-at "\\([_[:alnum:]]+\\.\\)*[_[:alnum:]]+")
    (let* ((string-at-point (match-string-no-properties 0))
           (selection (completing-read "Library name?"
                                      (alight-sublibraries string-at-point)
                                      nil nil string-at-point)))
      (when selection
        (alight-find-library selection)))))

;; --------------------------------------------------
;; Finding and parsing auxiliary functions

(defun alight--find-last-word-beginning (&optional point)
  "Find the last word and return the point where it start.
The POINT optional parameter is the point where to start searching the last
word."
  (unless point
    (setq point (point)))
  (save-excursion
    (goto-char point)
    (left-word)
    (while (and (> (point) (point-min))
                (string-equal "_"
                              (buffer-substring-no-properties (1- (point)) (point))))
      (left-word))
    (point)))

(defun alight--remove-non-words (str)
  "Remove any non leter or number of STR."
  (if (string-match "\\(^[[:alnum:]_]+\\)\\([^[:alnum:]]+\\)?$" str)
      (match-string 1 str)
    str))

(defun alight--find-last-word (&optional point)
  "Find the last word and return the string found with the beginning point.

Return a list: (WORD-START WORD)."
  (unless point
    (setq point (point)))
  (let ((last-word-start (alight--find-last-word-beginning)))
    (list last-word-start
          (alight--remove-non-words
           (buffer-substring-no-properties last-word-start point)))))

(defun alight--find-last-word-end (&optional point)
  "Find the last word and return the string and the ending point.

Return a list: (WORD-END WORD)."
  (save-excursion
    (let ((last-word (alight--find-last-word point)))
      (goto-char (car last-word))
      (search-forward-regexp "[[:space:]]*$" nil t)
      (list (- (match-beginning 0) 1)
            (cdr last-word)))))

(defun alight--subprogram-name (definition)
  "Return the subprogram name from the DEFINITION string.
With the procedure or function definition, return the name/identifier used."
  (when (string-match
         "\\(procedure\\|function\\)[[:space:]]+\\([[:alnum:]_]+\\)\\(;\\|[[:space:]]+\\)"         
         definition)
    (match-string-no-properties 2 definition)))

(defun alight--has-comment (string)
  "Return true if the STRING contains a comment."
  (string-match-p "--" string))

(defun alight-comment-at-point-p (&optional point)
  "Return true if at the POINT is a comment.
If POINT is not provided, then use the current point."
  (save-excursion
    (when point
      (goto-char point))
    (if (search-backward "--" (point-at-bol) t)
        t
      nil)))

(defun alight-string-at-point-p (&optional point)
  "Return true if there is a string at POINT.
If POINT is not provided, use the current point."
  (save-excursion
    (when point
      (goto-char point))
    (if (or (equal 'font-lock-string-face (get-text-property (- (point) 1)
                                                             'face))
            (looking-back "'.'?" 3)
            (search-backward-regexp "[( ]\"" (point-at-bol) t))
        t
      nil)))

(defun alight--find-last-indent (&optional point)
  "Return the amount of spaces used in the last indentation.
The POINT is the position where to start to look for a non-empty line
backwards.  If not provided, use the current position."
  (save-excursion
    (when point
      (goto-char point))
    (goto-char (point-at-bol))
    (if (re-search-backward "^[[:space:]]*[^ \n]" nil t)
        (current-indentation)
      0)))

(defconst alight-subprogram-regexp
  (concat "\\(overriding[[:blank:]]+\\)?"
          ;; procedure/function
          "\\(procedure\\|function\\)[[:blank:]]+"
          ;; Name
          "\\([[:alnum:]_]+\\)[[:blank:]]*"
          ;; Parameters
          "([^)]+)[[:blank:]]*"
          ;; Returns
          "\\(return[[:blank:]]+[[:alnum:]_]+[[:blank:]]*\\)?"
          ;; End definition (";")  or begin implementation ("is")
          "\\(;\\|is\\)")
  "Subprogram regular expression used to find them and their parts.")

(defun alight-subprogram-at-point ()
  "Return the subprogram declaration at point.
If there is no subprogram at point, return nil.

It start searching from the beginning of line."
  (save-excursion
    (goto-char (point-at-bol))
    (when (re-search-forward alight-subprogram-regexp nil t)
      (buffer-substring-no-properties (match-beginning 0) (match-end 0)))))
;; --------------------------------------------------
;; Goto functions

(defun alight-goto-package-specification ()
  "Open this package specification (the .ads file)."
  (interactive)
  (find-file-other-window
   (concat (file-name-sans-extension (buffer-file-name)) ".ads")))

(defun alight-goto-package-body ()
  "Open this package body (.adb) file."
  (interactive)
  (find-file-other-window
   (concat (file-name-sans-extension (buffer-file-name)) ".adb")))

;; --------------------------------------------------
;; Inserting functions

(defun alight-insert-end (&optional point)
  "Add \"end name\" where name is the package, procedure o function name.
The POINT parameter indicates where the \"end\" should be inserted.  If not
provided, then use the current position."
  (interactive)
  (unless point
    (setq point (point)))
  
  (if (re-search-backward
       "\\(package\\|function\\|procedure\\)[ ]+\\([^( ]+\\)" nil t)
      (progn (goto-char point)
             (insert "end " (match-string-no-properties 2) ";"))
      (message "Name not found!")))

(defconst alight--subprogram-regexp "\\(procedure\\|function\\)[[:blank:]]*\\([[:alnum:]_]+\\)"
  "Regular expression to find subprograms.")

(defun alight-find-all-subprograms ()
  "Return an alist with all subprograms names and their position.

This function ignores sub-subprograms.

Return an alist with (NAME . CHAR-POSITION) elements."
  (let ((lst-results nil))
    (save-excursion
      (goto-char (point-min))
      
      ;; Find all subprograms and save its position too.
      (while (search-forward-regexp alight--subprogram-regexp nil t)
        (push (cons (match-string 2) (match-beginning 0)) lst-results)        
        ;; Skip until the end of the subprogram.
        ;; (Thanks Ada to have the "end SUBPROGRAM-NAME;" in your
        ;; standard!)
        (search-forward-regexp (format "end[[:blank:]]+%s[[:blank:]]*;" (match-string 2))
                               nil t)))
    lst-results))

(defun alight-find-subprogram-position (subprogram-name)
  "Find where the subprogram body should be placed.

Search for all subprograms, and find the position where the subprogram
implementation by name SUBPROGRAM-NAME should be placed."
  (let* ((lst-subprograms (alight-find-all-subprograms))
         ;; Add the query as another option.
         ;; Sort the alist.
         (lst-subprograms2 (sort (push (cons subprogram-name 0)
                                       lst-subprograms)
                               (lambda (a b)
                                 (string-lessp (car a) (car b))))))
    ;; Return the position (not the subprogram name) of the next
    ;; subprogram positioned after the SUBPROGRAM-NAME inserted before.
    (cdr 
     (nth (+ 1
             ;; Find the position of SUBPROGRAM-NAME in the sorted alist.
             (cl-position-if (lambda (elt)
                               (string= (car elt) subprogram-name))
                             lst-subprograms))
          lst-subprograms))))

(defun alight--insert-subprogram-template (definition)
  ;; Insert and indent the template.
    (let ((indent-beginning (point))
          (counttabs (or (alight--find-last-indent) 4)))
      (insert (format "\n%s is \nbegin\n\nend %s;\n\n"
                      definition
                      (alight--subprogram-name definition)))
      (indent-region indent-beginning (point) counttabs)))

(defun alight-insert-subprogram-body (&optional char-position)
  "Insert the current subprogram definition at the body.
Go to the package body adb file and insert the current procedure or function
definition.  Add \"begin\" and \"end\" as required.

If CHAR-POSITION is provided, go to that position before inserting."
  (interactive)
  (let ((definition (substring (alight-subprogram-at-point) 0 -1)))
    ;; Go to the end of the body document.
    (alight-goto-package-body)
    (goto-char (or char-position
                   (alight-find-subprogram-position
                    (alight--subprogram-name definition))))
    ;; (search-backward-regexp "end [[:alnum:]_\\\\.]+;" nil t)
    ;; (goto-char (match-beginning 0))
    (goto-char (point-at-bol))

    (alight--insert-subprogram-template definition)

    ;; move the cursor to a proper position.
    (forward-line -3)
    (indent-line-to (+ counttabs standard-indent))))

(defun alight-insert-subprogram (definition)
  "Insert the current subprogram definition at the body.
Go to the package body adb file and insert the current procedure or function
definition.  Add \"begin\" and \"end\" as required.

DEFINITION is the subprogram specification (like in *.ads files)."
  (interactive "sSubprogram definition (end with \";\"):")
  ;; Go to the end of the body document.
  (goto-char (alight-find-subprogram-position
              (alight--subprogram-name definition)))
  (goto-char (point-at-bol))

  (alight--insert-subprogram-template definition)

  ;; move the cursor to a proper position.
  (forward-line -3)
  (indent-line-to (+ counttabs standard-indent)))


;; --------------------------------------------------
;; Fixing style

(defun alight-fix-case-at-point (&optional point)
  "Fix the case at the current point.
The POINT parameter is the point to apply.  If not speccified, use the current
point."
  (interactive)
  (unless point
    (setq point (point)))
  (unless (or (alight-comment-at-point-p (car (alight--find-last-word-end point)))
              (alight-string-at-point-p (car (alight--find-last-word-end point))))
    (let* ((last-word-list (alight--find-last-word point))
           (last-word-start (car last-word-list))
           (last-word (cadr last-word-list)))
      (if (member (downcase last-word) alight-reserved-words)
          (downcase-region last-word-start point)
        (upcase-initials-region last-word-start point)))))

(defun alight-fix-style-space-before-colon ()
  "Add only one space between a colon."
  (interactive)
  
  (save-excursion
    (goto-char (point-min))
    
    (while (search-forward-regexp "\\([^ ]\\):" nil t)
      (unless (alight--has-comment
               (buffer-substring-no-properties (point-at-bol) (point)))
        (replace-match "\\1 :")))))


(defun alight-fix-style-space-after-colon ()
  "Add only one space between a colon."
  (interactive)
  
  (save-excursion
    (goto-char (point-min))
    
    (while (search-forward-regexp ":\\([^ =]|$\\)" nil t)
      (unless (alight--has-comment
               (buffer-substring-no-properties (point-at-bol) (point)))
        (replace-match ": \\1")))))

(defun alight-fix-style-replace-tabs ()
  "Replace all tabs with spaces."
  (interactive)
  ;; Replace tabs
  (save-excursion
    (goto-char (point-min))
    (replace-regexp "\t" "       ")))

(defun alight-fix-style-comments-with-two-spaces ()
  "Ensure that all comments begins with two spaces.
Leave all comments with two spaces between the starting -- characters and
the text."
  (interactive)  
  ;; Comments must have two spaces only
  (save-excursion
    (goto-char (point-min))
    (replace-regexp "--[[:space:]]\\([^ ]\\)" "--  \\1")))

(defun alight-fix-style-empty-lines ()
  "Remove spaces in all empty lines."
  (interactive)
  ;; Lines with spaces -> empty lines
  (save-excursion
    (goto-char (point-min))
    (replace-regexp "^[[:space:]]+$" "")))

(defun alight-fix-style-remove-trailing-spaces ()
  "Remove all trailing spaces."
  (interactive)
  ;; Remove trailing spaces.
  (save-excursion
    (goto-char (point-min))
    (replace-regexp "\\([^ ]\\) +$" "\\1")))

(defun alight-fix-style-space-before-parenthesis ()
  "Leave a space before each parenthesis."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (replace-regexp "\\([^ ]\\)(" "\\1 (")))

(defun alight-fix-style-space ()
  "Fixes some space style problems"
  (interactive)

  (save-excursion
    (alight-fix-style-replace-tabs)

    ;; TODO: Ignore colon in strings
    ;; (gnat-space-before-colon)
    ;; (gnat-space-after-colon)

    (alight-fix-style-comments-with-two-spaces)
    (alight-fix-style-empty-lines)
    (alight-fix-style-remove-trailing-spaces)

    ;; TODO: Ignore parenthesis in strings.
    ;; (gnat-space-before-parenthesis)
    
    ;; (goto-char (point-min))
    ;; (query-replace-regexp ":\\([^ =]|$\\)" ": \\1")
    )
  ) ;; defun

(defun alight-fix-style ()
  "Fix the Ada usual style for the current buffer.

This is function added to the `before-save-hook' to fix common style problems."
  (alight-fix-style-space)) ;; defun


;; --------------------------------------------------
;; Compilation commands

(defun alight--directory-all-parents (dir)
  "Return all parents from DIR.
Return a list of all parent directory from DIR."
  (let ((curdir dir)
        (result '()))
    (while curdir
      (push curdir result)
      (setq curdir (file-parent-directory curdir)))
    result))

(defun alight--find-makefile ()
  "Find the makefile directory.
Search for a Makefile file from this directory and their parent of the parent."
  (locate-file "Makefile" (alight--directory-all-parents (expand-file-name default-directory))))

(defun alight-make-ada ()
  "Call the compilation program.
Search for a Makefile file and then call `compile' with its path."
  (interactive)
  (let ((makefile-path (alight--find-makefile)))
    (if makefile-path
        (compile (format "make -C '%s' " (file-name-directory makefile-path)))
      (message "Makefile not found. Is this buffer file in the correct folder?"))))
                   

(provide 'alight-commands)

;;; alight-commands.el ends here
